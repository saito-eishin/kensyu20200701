// 社員情報登録用の入力チェック
function testData(){
    var temp_simei = document.entry.simei.value;
    //文字列の中から空白を取り除く処理
    temp_simei = temp_simei.replace(/\s+/g,"");
    n = temp_simei.length;
    document.entry.simei.value = temp_simei;
    if(temp_simei==""){    //名前が入っているかのチェック
        alert("名前を入力してください。"); //入っていない場合、エラーダイアログを表示
        return false;
    }else if(n >30 ){
        alert('名前は30文字以内で入力してください。')
        return false;
    }

    var temp_pref = document.entry.pref.value;
    if(temp_pref == ""){
        alert("都道府県を選択してください。");
        return false;
    }

    var temp_age = document.entry.age.value;
    if(temp_age==""){
        alert("年齢を入力してください");
        return false;
    }else if(temp_age < 1 || temp_age > 99){
        alert("1～99歳の間で入力してください。");
        return false;
    }

    if(window.confirm('社員情報の登録を行います。よろしいですか？')){
          document.entry.submit();
    }
}
