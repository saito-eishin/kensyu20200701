<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<!-- デバッグプリント
<pre>
<?php
    var_dump($_GET);
?>
</pre> -->
<?php
    //都道府県、性別の連想配列を取り込む
    include('./include/statics.php');

    // common
    // include('./include/functions.php');
    $DB_DSN = 'mysql:host=localhost; dbname=esaito; charset=utf8';
    $DB_USER = 'webaccess';
    $DB_PW = 'toMeu4rH';
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
    // $pdo = initDB();

    //error_flag=1のときのUndefinedErrorを回避するために、$param_idをNullと定義する
        $param_id = "";
        //存在チェックと、エラーフラグを設定
        if (isset($_GET['member_ID']) && $_GET['member_ID'] !='') {
            $param_id = $_GET['member_ID'] ;
            $id_error_flag = 0 ;
        }else{$id_error_flag = 1;
        }

        $param_name = "";
        if (isset($_GET['na']) && $_GET['na'] !='') {
            $param_name = $_GET['na'];
        }

    //DBから持ってくるの構文を$
    //WHERE条件に指定しているメンバーIDのパラメータは、$_GETの値を$param_idに格納したもの
        $query_str = "SELECT m.member_ID AS mid,
                             m.name AS na,
                             m.pref AS pr,
                             m.seibetu AS sei,
                             m.age,
                             sm.section_name AS sna,
                             gm.grade_name AS gna

                             FROM member AS m
                             LEFT JOIN section1_master AS sm ON m.section_ID = sm.ID
                             LEFT JOIN grade_master AS gm ON m.grade_ID = gm.ID

                             WHERE `member_ID`=" . $param_id;
                             /*直打ちだと、WHERE 'member_ID' ='XX' ; */

    //デバッグプリント
    // echo $query_str;
        $sql = $pdo->prepare($query_str);
        $sql->execute();
        $result = $sql->fetchAll();

    //検索結果が0件、データが2件以上ヒットする場合のフラグ設定
    //detail01.phpの検索結果は常に1件なはず・・・
    //$id_error_flagとは別の変数を作った
        if(count($result) == 1){
            $count_error_frag = 0;
        }elseif(count($result) !== 1){
            $count_error_frag = 1;
        }

    //デバッグプリント
        // echo count($result);
        // echo $query_str;
    ?>
<script type ='text/javascript'>
// 削除ダイアログ処理
function deldata(){
    if(window.confirm('<?php echo $result[0]['na'];?>さんの社員情報を削除します。よろしいですか。')){
        document.sakujyo.submit();
    }
}
</script>

<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>社員情報詳細</title>
    </head>
    <body>
        <?php
        if($id_error_flag == 0 AND $count_error_frag == 0){
        ?>
        <?php include('./include/header.php');?>
        <div id='searchform'>
            <h2>社員情報詳細</h2>
            <hr/>
            <table border='1' class='tabledesign'>
                <tr>
                    <th class="tablea" width ='150' >社員ID</th>
                    <td width ='300' align ='left'><?php echo $result[0]['mid']; ?></td>
                </tr>
                <tr>
                    <th class="tablea">名前</th>
                    <td><?php echo $result[0]['na']; ?></td>
                </tr>
                <tr>
                    <th class="tablea">出身地</th>
                    <!-- 出身地が都道府県デフォルトの場合は空で返す -->
                    <td><?php if($result[0]['pr'] =='0'){echo "";}else{ echo $pref_array[$result[0]['pr']];} ?></td>
                </tr>
                <tr>
                    <th class="tablea">性別</th>
                    <td><?php echo $gender_array[$result[0]['sei']]; ?></td>
                </tr>
                <tr>
                    <th class="tablea">年齢</th>
                    <td> <?php echo  $result[0]['age']; ?></td>
                </tr>
                <tr>
                    <th class="tablea">所属部署</th>
                    <td> <?php echo $result[0]['sna']; ?></td>
                </tr>
                <tr>
                    <th class="tablea">役職</th>
                    <td> <?php echo $result[0]['gna']; ?></td>
                </tr>
            </table>

            <div style ="display:inline-flex">
            <!-- もう一つの書き方 -->
            <!-- <form method = 'POST' action ='./entry_update01.php?member_ID=<?php echo $result[0]['mid'] ?>'> -->
        <form method ='POST' action ='./entry_update01.php' name='hensyu'>
            <input type ='hidden' name ='member_ID' value ='<?php echo $result[0]['mid'];?>'>
            <input type='submit' value ='編集'>
        </form>
        <form method = 'POST' action ='./delete01.php' name='sakujyo'>
            <input type='hidden' name ='member_ID' value='<?php echo $result[0]['mid'] ?>'>
            <input type='button' id='sakujyo_b' value='削除' onClick='deldata();'>
        </form>
            </div>
        <?php
        }
        else{
            echo "エラーが発生しました。<br/>
                 <a href='./index.php'>[トップ画面に戻る]</a>";
        }
        ?>
        </div>
    <!-- デバッグプリント
    <pre>
    <?php
    var_dump($result)
    ?>
    </pre> -->
    </body>
</html>
