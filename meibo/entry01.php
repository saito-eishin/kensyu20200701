<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <script src="./include/function.js"></script>
        <title>社員名簿システム</title>
    </head>
    <body>
<?php
// common
// include('./include/functions.php');
    include('./include/function.php');
    include('./include/statics.php');
    include('./include/header.php');
?>
        <form method ='POST' action ='./entry02.php' name='entry'>
        <div id='searchform'>
        <h2>新規社員登録</h2>
        <hr/>
        <table border='1' class='tabledesign'>
            <tr>
                <th class="tablea" align = 'center' width = '300'>名前</th>
                <td><input type='text'name ='simei' width= '150' max='30' maxlength='30' placeholder="名前を入力してください。"><span class="must"> ※必須です</span></td>
            </tr>
            <tr>
                <th class="tablea" align = 'center'>出身地</th>
                <td><select name ='pref'>
                    <option value =''>都道府県</option>
                    <?php
                         foreach ($pref_array as $key => $value){
                                  echo "<option value =" . $key . ">" . $value . "</option>";
                         }
                    ?>
                </select><span class="must"> ※必須です</span></td>
            </tr>
            <tr>
                <th class="tablea" align = 'center'>性別</th>
                <td><input type ='radio' name='seibetu' value='1' checked>男
                    <input type ='radio' name='seibetu' value='2'>女
                </td>
            </tr>
            <tr>
                <th class="tablea" align = 'center' width = '200'>年齢</th>
                <td><input type='number' name ='age' size='15' min='1' max='99' value=''>才<span class="must"> ※必須です</span></td>
            </tr>
            <tr>
                <th class="tablea" align = 'center'>所属部署</th>
                <td width='800'>
                    <?php
                        foreach(getSection() as $each){
                            echo "<input type='radio' id='sec" . $each['ID']
                            . "' name='busyo' value='" . $each['ID'] . "'";
                            if($each['ID'] == "1") echo " checked ";
                            echo "><label for='sec" . $each['ID'] . "'>" . $each['section_name'] . "</label>";
                        }
                    ?>

                </td>
            </tr>
            <tr>
                <th class="tablea" align = 'center'>役職</th>
                <td>
                    <?php
                       foreach(getGrade() as $each){
                           echo "<input type ='radio' id='grd" . $each['ID']
                           . "' name='yakusyoku' value= '" . $each['ID']."'";
                          if($each['ID']== '1') echo "checked";
                           echo "><lavel for='grd" . $each['ID'] . "'>" . $each['grade_name'] . "</lavel>";
                         }
                      ?>
                </td>
            </tr>
        </table>
        <input type = 'button' value='登録' onClick='testData();'>
        <input type='reset' value='リセット'>
        </form>
        </div>
        <!-- <pre>
        <?php
        var_dump()
         ?>
        </pre> -->

    </body>
</html>
