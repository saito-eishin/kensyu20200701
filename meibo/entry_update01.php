<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<!-- <pre>
<?php
var_dump($_POST);
 ?>
</pre> -->

<?php

// common
// include('./include/functions.php');
    $DB_DSN = 'mysql:host=localhost; dbname=esaito; charset=utf8';
    $DB_USER = 'webaccess';
    $DB_PW = 'toMeu4rH';
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

    include('./include/statics.php');
    include('./include/function.php');
    $param_id = "";
    if (isset($_POST['member_ID']) && $_POST['member_ID'] !='') {
        $param_id = $_POST['member_ID'] ;
    }
    // $pdo = initDB();
    $query_str = "SELECT m.member_ID AS mid,
                         m.name AS na,
                         m.pref AS pr,
                         m.seibetu AS sei,
                         m.age,
                         m.section_ID,
                         m.grade_ID

                         FROM member AS m

                         WHERE `member_ID`=".$param_id;
    //updateのSQL文メモ
    // $query_str ="UPDATE member
    //              SET
    //              member_ID = '1',
    //              name ='伊藤博文',
    //              pref ='1',
    //              age,
    //              section_ID,
    //              grade_ID
    //              WHERE
    //              member.member_ID =" . $param_id;

    //デバッグプリント
       // echo $query_str;
       $sql = $pdo->prepare($query_str);
       $sql->execute();
       $result = $sql->fetchAll();

?>


<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>社員情報詳細</title>
        <script src="./include/function2.js"></script>
    </head>
    <body>
        <?php include('./include/header.php'); ?>
        <div id='searchform'>
        <h2>社員情報編集</h2>
        <hr/>
        <form method ='POST' action ='./entry_update02.php' name='entry'>
        <table border='1' class='tabledesign'>
                <tr>
                    <th class="tablea" width ='150'>社員ID</th>
                    <td width ='300' align ='left'><?php echo $result[0]['mid']; ?></td>
                </tr>
                <tr>
                    <th class="tablea">名前</th>
                    <td><input type ='text' name ='simei' width= '150' max='30' maxlength='30' value ='<?php echo $result[0]['na']; ?>'><span class="must"> ※必須です</span></td>
                </tr>
                <tr>
                    <th class="tablea">出身地</th>
                    <td><select name ='pref' value ='syussin'>
                        <?php
                            foreach ($pref_array as $key => $value){
                                if($result[0]['pr'] == $key){
                                    echo "<option value='". $key . "' selected>" . $value . "</option>";
                                }else {
                                    echo "<option value='". $key ."'>". $value . "</option>";
                                }
                            }
                        ?>
                        </select><span class="must"> ※必須です</span></td>
                </tr>
                <tr>
                    <th width='300' class="tablea">性別</th>
                    <td><input type ='radio' name='seibetu' value='1' <?php if($result[0]['sei'] == "1"){echo "checked";}?>>男
                        <input type ='radio' name='seibetu' value='2' <?php if($result[0]['sei'] == "2"){echo "checked";}?>>女</td>
                </tr>
                <tr>
                    <th class="tablea">年齢</th>
                    <td><input type ='number' name='age' size ='15' min='1' max='99' value = '<?php echo  $result[0]['age']; ?>'>才<span class="must"> ※必須です</span></td>
                </tr>
                <tr>
                    <th class="tablea">所属部署</th>
                    <td width ='800'>
                        <?php
                        foreach(getSection() as $each){
                            echo "<input type='radio' id='sec" . $each['ID']
                                . "' name='busyo' value='" . $each['ID'] . "'";
                                    if($each['ID'] == $result[0]['section_ID']) echo " checked ";
                                        echo "><label for='sec" . $each['ID'] . "'>" . $each['section_name'] . "</label>";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th class="tablea">役職</th>
                    <td>
                        <?php
                        foreach(getGrade() as $each){
                           echo "<input type='radio' id='grd" . $each['ID']
                                . "' name='yakusyoku' value='" . $each['ID'] . "'";
                                    if($each['ID'] == $result[0]['grade_ID']) echo " checked ";
                                        echo "><label for='grd" . $each['ID'] . "'>" . $each['grade_name'] . "</label>";
                        }
                        ?>

                    </td>
                </tr>
            </table>
            <input type = 'hidden' name='member_ID' value='<?php echo $result[0]['mid'];?>'>
            <input type = 'button' value='登録' onClick='testData();'>
            <input type = 'reset' value='リセット'>
        </form>
    </div>
        <!-- デバッグプリント
        <pre>
            <?php
                var_dump($result)
                ?>
        </pre>
        -->
    </body>
</html>
