<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">

<?php
include('./include/function.php');
// $pdo = initDB();

    //パラメータチェックを行う
    $param_simei = '';
    if (isset($_GET['simei']) ) {
        $param_simei = $_GET['simei'];
    }

    $param_seibetu = '';
    if (isset($_GET['seibetu'])) {
        $param_seibetu = $_GET['seibetu'];
    }

    $param_busyo = '';
    if (isset($_GET['busyo'])) {
        $param_busyo = $_GET['busyo'];
    }

    $param_yakusyoku = '';
    if (isset($_GET['yakusyoku'])) {
        $param_yakusyoku = $_GET['yakusyoku'];
    }
    // common
    $DB_DSN = 'mysql:host=localhost; dbname=esaito; charset=utf8';
    $DB_USER = 'webaccess';
    $DB_PW = 'toMeu4rH';
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
    // $pdo = initDB();

    $query_str ="SELECT m.member_ID AS mid,
                         m.name AS na,
                         sm.section_name AS sna,
                         gm.grade_name AS gna

                 FROM member AS m
                 LEFT JOIN section1_master AS sm ON m.section_ID = sm.ID
                 LEFT JOIN grade_master AS gm ON m.grade_ID = gm.ID

                 WHERE 1=1 ";

    //SQL検索条件どおりのデータを返す処理の実装
    if($param_simei != ''){
            $query_str .=" AND m.name LIKE '%". $param_simei ."%'";
    }

    if ($param_seibetu != ''){
            $query_str .= " AND m.seibetu =" . $param_seibetu ;
    }

    if ($param_busyo != ''){
            $query_str .=" AND sm.ID =" . $param_busyo ;
    }

    if ($param_yakusyoku != ''){
            $query_str .=" AND gm.ID =" . $param_yakusyoku ;
    }

        //デバッグプリント
        // echo $query_str;
        $sql = $pdo->prepare($query_str);
        $sql->execute();
        $result = $sql->fetchAll();

?>

<script type ='text/javascript'>
    function clearReset(){
        document.top.simei.value = ''; //検索のリセットを初期値に戻す。ここではnullにしている。
        document.top.seibetu.value = '';
        document.top.busyo.value = '';
        document.top.yakusyoku.value = '';
    }
</script>

<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>社員名簿システム</title>
    </head>
    <body>
        <form method ='GET' action ='./index.php' name='top'>

        <?php include('./include/header.php'); ?>
    <div id='searchform'>
        <table>
            <tr><th>名前：</th>
                <td><input type='search' font-size='20' name='simei' value ='<?php if(isset($param_simei)){ echo $param_simei;} ?>'></td>
            </tr>
        </table>
        <table>
            <tr>
                <th>性別：</th>
                <td><select name='seibetu' value='gender'>
                    <option value='' >すべて</option>
                    <option value='1' <?php  if($param_seibetu == "1"){ echo "selected";} ?> >男性</option>
                    <option value='2' <?php  if($param_seibetu == "2"){ echo "selected";} ?> >女性</option>
                    </select>
                </td>
                <th>   </th>
                <th>部署: </th>
                <td><select name='busyo' value='department'>
                    <option value=''>すべて</option>
                    <?php
                      $result_section = getSection();
                      foreach($result_section as $each){
                        if($_GET['busyo'] == $each['ID']){
                          echo "<option value= '" . $each['ID'] . "'selected>" . $each['section_name'] . "</option>";
                        } else {
                          echo "<option value= '" . $each['ID'] . "'>" . $each['section_name'] . "</option>";
                        }
                      }
                     ?>
                    </select>
                </td>
                <th>   </th>
                <th>役職: </th>
                    <td><select name='yakusyoku' value='role'>
                        <option value=''>すべて</option>
                        <?php
                           $result_grade = getGrade();
                           foreach($result_grade as $each){
                             if($_GET['yakusyoku'] == $each['ID']){
                               echo "<option value= '" . $each['ID'] . "'selected>" . $each['grade_name'] . "</option>";
                             }else{
                               echo "<option value= '" . $each['ID'] . "'>" . $each['grade_name'] . "</option>";
                             }
                           }
                          ?>

                        </select>
                    </td>
            </tr>
        </table>
        <table>
            <br/>
        <div id='serchexact'>
            <input type ='submit' value='検索'>
            <input type ='button' value='リセット' onclick='clearReset()'>
        </div>
        <!--
        // if ($_GET['seibetu'] = 1 ) {
        //     echo  '性別が「男性」で検索されています。';
        // }elseif($_GET['seibetu'] = 2 ){
        //           echo  '性別が「女性」で検索されています。';
        // }
        //seibetu のvalueに99を定義。issetの条件に != 99 を追加すれば動く
        -->
        </table>
        <hr/>
        検索結果：<?php echo count($result); ?>
        <table border="1" class='tabledesign'>
            <tr>
                <th class="tablea" width = '80'>社員ID</th>
                <th class="tablea" width = '300'>名前</th>
                <th class="tablea" width = '250'>部署</th>
                <th class="tablea" width = '250'>役職</th>
            </tr>
        <?php
        foreach($result as $each){
            //var_dump($each);
            echo  "<tr><td align ='left'>" . $each["mid"]       . "</td>"
                . "<td align ='center'><a href='./detail01.php?member_ID=" . $each["mid"] . "'>" . $each["na"] . "</td></a>"
                . "<td align ='center'>"   . $each["sna"]       . "</td>"
                . "<td align ='center'>"   . $each["gna"]       . "</td></tr>";
        }

        if (count($result) == 0)
            echo "<td colspan = 4 id='serchexact'>" ."検索結果なし" ."</td>" ;
         ?>
        </table>
    </div>
    </form>
        <!-- <pre>
            <?php
            var_dump($result);
             ?>
        </pre> -->

    </body>
</html>
