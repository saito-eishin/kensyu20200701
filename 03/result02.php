<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ログイン結果画面ver.2</title>
    </head>
    <body>
        <h1>ログイン結果ver.2</h1>
        <?php
        //ゴールド会員
        $gold_userid = 'test1';
        $gold_pass ='xx1';
        $gold_message ='<br/>あなたはゴールド会員です。';

        //ノーマル会員
        $nomal_userid = 'test2';
        $nomal_pass ='yy1';
        $nomal_message='<br/>あなたはノーマル会員です。';

        if($_POST['userid'] == $gold_userid AND $_POST['pass'] == $gold_pass){
            /*ゴールド会員ログイン成功の処理 */
            echo 'ログインに成功しました'.$gold_message;
        }elseif($_POST['userid'] == $nomal_userid AND $_POST['pass'] == $nomal_pass){
             //ノーマル会員ログイン成功
            echo 'ログインに成功しました'. $nomal_message;
        } else{
            // ログイン失敗の処理
            echo 'ログインに失敗しました<br/>IDまたはパスワードが違います。';
        }
        ?>
        <br><br><br><br>
        <a href="login02.php">[ログイン画面に戻る]</a>
    </body>
</html>
