<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>繰り返し処理</title>
    </head>
    <body>
        <h1>繰り返し処理</h1>
        <form method="GET" action="loop01.php">
            <input type='number' name='gyousuu'>行のテーブルを生成する
            <br/>
            <input type='submit' value='送信'>
            <input type='reset' value='リセット'>
            <hr>
            <table border="1">
                <?php
                    for ($i=0; $i < $_GET['gyousuu']; $i++) {
                        echo '<tr>
                                <td>梶谷</td>
                                <td>乙坂</td>
                                <td>楠本</td>
                                <td>神里</td>
                                <td>筒香</td>
                              </tr>';
                    }
                ?>
             </table>
        </form>
        <!--多重ルームの理解用メモ -->
        <?php
        for($i = 1; $i < 10; $i++){
            for($j = 1; $j < 10; $j++){ //$iじゃなくて、$jであることに注意！

                echo $i;
                echo $j; //改行が無いと続けて表示されます。
                echo "<br>"; //改行
            }
        }
        ?>
    </body>
</html>
