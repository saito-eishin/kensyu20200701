<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>繰り返し処理3</title>
    </head>
    <body>
        <h1>カウントした文字列を表示する</h1>
        <form method="GET" action="loop03.php">
            <input type='number' name='gyousuu' size="3" width="5">行 ×
            <input type='number' name='retusuu' size="3" width="5">列
            <br/>
            <input type='submit' value='送信'>
            <input type='reset' value='リセット'>
            <hr>
            <table border="1">
                <?php
                    for ($a=1; $a <= $_GET['gyousuu']; $a++) {
                            echo  '<tr>';
                        for ($b=1; $b <= $_GET['retusuu']; $b++) {
                            echo  '<td>'.$a.'-'.$b.'</td>';
                        }
                            echo  '</tr>';
                        }
                ?>
             </table>
        </form>
    </body>
</html>
