<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>繰り返し処理2</title>
    </head>
    <body>
        <h1>行数と列数を指定してテーブルを書いてみよう</h1>
        <form method="GET" action="loop02.php">
            <input type='number' name='gyousuu' size="3" width="5">行 ×
            <input type='number' name='retusuu' size="3" width="5">列
            <br/>
            <input type='submit' value='送信'>
            <input type='reset' value='リセット'>
            <hr>
            <table border="1">
                <?php
                    for ($i=0; $i < $_GET['gyousuu']; $i++) {
                            echo  '<tr>';
                        for ($x=0; $x < $_GET['retusuu']; $x++) {
                            echo  '<td>テスト列</td>';
                        }
                            echo  '</tr>';
                        }
                ?>
             </table>
        </form>
    </body>
</html>
