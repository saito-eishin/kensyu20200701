<!DOCTYPE html>
<html>
    <head>
        <meta charset ='utf-8'>
        <mete name ='viewport' content ='width=device-width','initial-scale=1'>
        <title>連想配列の練習その2</title>
    </head>
    <body>
        <h1>連想配列の練習その2</h1>
    <pre>
    <?php
//すべての要素を出力する
    $me_data = array(
        'fruit' => 'スイカ',
        'sport' => '野球',
        'town' => '横浜',
        'age' => 21,
        'food' => 'カレーライス'
        );

    foreach($me_data as $each){
        echo $each . '<br/>';
        }


        foreach($me_data as $key => $value){
            echo $key . ' : ' . $value . '<br/>';
        }

    var_dump($me_data);
     ?>

    <!--表で表現してみる-->
    <?php
    $me_data = array(
        'fruit' => 'スイカ',
        'sport' => '野球',
        'town' => '横浜',
        'age' => 21,
        'food' => 'カレーライス'
        );

    foreach($me_data as $each){
        echo $each . '<br/>';
        }

        $border1 = '<table border = "1" width="100"><tr align="left"><th>';
        $border2 = '</th><td>';
        $border3 = '</td></tr></table>';
        foreach($me_data as $key => $value){
            echo $border1.$key.$border2.$value.$border3;
        }

      ?>
    </pre>
    <!--htmlで表を書いてみる-->
    <table border="1">
        <tr>
            <th>
                aiueo
            </th>
            <td>
                igejoge
            </td>
        </tr>
    </table>

    <!-- 重要：第4回講義メモ
    フォームの中身が連想配列になっている
    anketo.htmlの中身をVar_dumpでとってくる意味
    echo $_GET['syumi'];
    でやると、「syumi」は配列なのでエラーになる
    echoは文字列しか出せず配列は出せない
    syumi内のPCのみを出力したいとき、
    echo $_GET['syumi'][0];
    とやるとだせる。0が文字列だから。
    多次元だと添え字がさらに配列の場合がある


    -->

    </body>
</html>
