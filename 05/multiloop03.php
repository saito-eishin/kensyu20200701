<?php
// データ構造を作る
$game1 = array(
    'name' => 'モンスターハンター ワールド',
    'date' => '2017/12/9',
    'company' => 'カプコン',
    'genre' => 'ARPG',
    'memo' => '新武器とアクションモーションが追加。ストーリーを進めると、過去作のモンスターが登場するワクワク感は本シリーズの強み。',
);
$game2 = array(
    'name' => 'コール オブ デューティ ウォーゾーン',
    'date' => '2020/3/10',
    'company' => 'アクティビジョン',
    'genre' => 'FPS',
    'memo' => 'まさに戦場を体感できる。いつどこから撃たれるかわからない。シリーズの中でも突出した緊張感を誇る。ドンカツしたときの達成感も相当なもの。',
);
$game3 = array(
    'name' => 'グランツーリスモSPORT',
    'date' => '2017/10/19',
    'company' => 'ポリフォニーデジタル',
    'genre' => 'ドライビングシミュレータ',
    'memo' =>'実際に車で運転しているかのようなリアリティがある。中でも世界一のサーキットコース「ニュルブルクリンク」を、F1カテゴリーで走行するのは爽快だ。',
);
$games = array($game1,$game2,$game3);
 ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>多次元配列の練習3</title>
    </head>
    <body>
        <h1>多次元配列の練習3</h1>
        <form method ='GET' action = 'multiloop03.php'>
        <table border ="1">
        <tr><th>タイトル</th>
        <th>発売日</th>
        <th>販売元</th>
        <th>ジャンル</th>
        <th>備考</th></tr>
        <?php
        //foreachで回す
        foreach ($games as $each) {
            echo  '<tr><td>'  . $each['name']     . '</td>'
                . '<td>'      . $each['date']     . '</td>'
                . '<td>'      . $each['company']  . '</td>'
                . '<td>'      . $each['genre']    . '</td>'
                . '<td>'      . $each['memo']     . '</td></tr>';
        }
        ?>
        </table>
        <pre>
        <?php
        var_dump($games);
        ?>
        </pre>
    </body>
</html>
