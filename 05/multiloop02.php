<!DOCTYPE html>
<html>
    <head>
        <meta charset ='utf-8'>
        <mete name ='viewport' content ='width=device-width','initial-scale=1'>
        <title>多次元配列の練習2</title>
    </head>
    <body>
        <h1>多次元配列の練習2</h1>
    <pre>
    <?php

    $player01 = array(
        'id' => '3',
        'name' => '梶谷隆幸',
        'position' => '外野手',
        'from' => '島根',
        'year' => '2007',
    );
    $player02 =  array(
        'id' => '44',
        'name' => '佐野恵太',
        'position' => '外野手',
        'from' => '岡山',
        'year' => '2017',
    );
    $player03 =  array(
        'id' => '15',
        'name' => '井納翔一',
        'position' => '投手',
        'from' => '東京',
        'year' => '2013',
    );

    $players = array($player01, $player02, $player03);

    //foreachで表す１
    foreach($player01 as $each ){
        echo $each . '<br/>';
        }

    //foreachで表す２

    foreach($players as $each){
        echo '背番号: ' . $each['id'] . ', '
            . '名前: ' . $each['name'] . ', '
            . 'ポジション: ' . $each['position'] . ', '
            . '出身地: ' . $each['from'] . ', '
            . '入団年: ' . $each['year'] . '<br/>';
        }

    ?>
   </pre>
   </body>
</html>
