<!DOCTYPE html>
<html>
    <head>
        <meta charset ='utf-8'>
        <mete name ='viewport' content ='width=device-width','initial-scale=1'>
        <title>多次元配列の練習1</title>
    </head>
    <body>
        <h1>多次元配列の練習１</h1>

<?php
//二次元配列
$game_a = array('パズドラ', 'モンスト', 'FGO');
$game_b = array('モンハン', 'Callofduty', 'ファイナルファンタジー');
$game_c = array('マリオ', 'ポケモン', 'スプラトゥーン');

$game_all = array($game_a, $game_b, $game_c);

echo '<pre>';
var_dump($game_all);
echo '</pre>';

foreach ($game_all as $each) {

    foreach($each as $value){
        echo $value.'<br/>';
    }
}


?>

<!--練習 https://zeropuro.com/blog/?p=473-->
<<?php
$x = array();
$y = array(1,3,5,7); //奇数の数値を格納
$z = array(2,4,6,8); //偶数の数値を格納

array_push($x,$y) ; //xにyを追加
array_push($x,$z) ; //xにzを追加
//foreachはfor文の様に繰り返す命令
foreach($x as $row){ //最初は$xの１行目のデータが$rowに入る

//このとき、$rowも配列であることを忘れずに。
foreach($row as $data){ //$rowの先頭要素から取得していく
    echo $data;
    echo "<br>";
}

}

 ?>
