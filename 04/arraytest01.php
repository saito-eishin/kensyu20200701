<!DOCTYPE html>
<html>
    <head>
        <meta charset ='utf-8'>
        <mete name ='viewport' content ='width=device-width','initial-scale=1'>
        <title>配列の学習</title>
    </head>
    <body>
        <h1>配列の学習</h1>

        <pre>
        <?php
        $fruit = array('りんご','すいか','みかん','なし','イチゴ','かき');

        echo $fruit[3].'<br/>';
        echo $fruit[0].'<br/>';
        echo $fruit[9].'<br/>';
        $fruit[3] = 'いちじく';
        echo $fruit[3].'<br/>';
        $fruit[6] = 'キウイ';
        echo $fruit[6].'<br/>';


        var_dump($fruit);

        $game = ['cod','apex','monsterhunter','darksoul'];

        echo $game[0].'<br/>';


        $fruit[0] = "りんご";
        $fruit[1] = "すいか";
        $fruit[2] = "みかん";
        $fruit[3] = "なし";
        $fruit[4] = "イチゴ";
        $fruit[5] = "かき";

        echo $fruit[5];
        ?>
        </pre>

        <!-- for文の利用 -->

        <?php
        $fruit = array('りんご','すいか','みかん','なし','イチゴ','かき');

        var_dump($fruit);

        for($i=0;$i<count($fruit);$i++){
            echo $fruit[$i].'<br/>';
        }

         ?>

         <!--foreachの利用 -->
         <?php
        $fruit = array('りんご','すいか','みかん','なし','イチゴ','かき');

        var_dump($fruit);

        foreach($fruit as $each){
            echo $each .'<br/>';

        }
          ?>

    </body>
</html>
