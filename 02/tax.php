<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>税計算</title>
    </head>
    <body>
        <h1>税計算画面</h1>
        <form method="POST" action="tax.php">
            <table border="1">
                <tr>
                    <th>商品名</th>
                    <th>価格(単位:円、税抜き)</th>
                    <th>個数</th>
                    <th>税率</th>
                </tr>
                <tr>
                    <td><input type='text' name='syouhinmei1' size="15"></td>
                    <td><input type='number' name='kakaku1' size="15"></td>
                    <td><input type='number' name='kosuu1' size="15">個</td>
                    <td>
                        <input type='radio' name='zeiritu1' size="15" value="8%"checked>8%
                        <input type='radio' name='zeiritu1' size="15" value="10%">10%
                    </td>
                </tr>
                <tr>
                    <td><input type='text' name='syouhinmei2' size="15"></td>
                    <td><input type='number' name='kakaku2' size="15"></td>
                    <td><input type='number' name='kosuu2' size="15">個</td>
                    <td>
                        <input type='radio' name='zeiritu2'size="15" value="8%"checked>8%
                        <input type='radio' name='zeiritu2'size="15" value="10%">10%
                    </td>
                </tr>
                <tr>
                    <td><input type='text' name='syouhinmei3' size="15"></td>
                    <td><input type='number' name='kakaku3' size="15"></td>
                    <td><input type='number' name='kosuu3' size="15">個</td>
                    <td>
                        <input type='radio' name='zeiritu3' size="15" value="8%"checked>8%
                        <input type='radio' name='zeiritu3'size="15" value="10%">10%
                    </td>
                </tr>
                <tr>
                    <td><input type='text' name='syouhinmei4' size="15"></td>
                    <td><input type='number' name='kakaku4' size="15"></td>
                    <td><input type='number' name='kosuu4' size="15">個</td>
                    <td>
                        <input type='radio' name='zeiritu4' value="8%"checked>8%
                        <input type='radio' name='zeiritu4' value="10%">10%
                    </td>
                </tr>
                <tr>
                    <td><input type='text' name='syouhinmei5' size="15"></td>
                    <td><input type='number' name='kakaku5' size="15"></td>
                    <td><input type='number' name='kosuu5' size="15">個</td>
                    <td>
                        <input type='radio' name='zeiritu5'size="15" value="8%"checked>8%
                        <input type='radio' name='zeiritu5'size="15" value="10%">10%
                    </td>
                </tr>
            </table>
            <input type='submit' value='送信' >
            <input type='reset' value='リセット'>
            <br/>
            <br/>
            <table border="1">
                <tr>
                    <th>商品名</th>
                    <th>価格(単位:円、税抜き)</th>
                    <th>個数</th>
                    <th>税率</th>
                    <th>小計(単位：円)</th>
                </tr>
                <tr> <!--<?=$POST['XX']?></td>も使える -->
                    <td><?=$_POST['syouhinmei1']?></td>
                    <td><?php echo $_POST['kakaku1'];?></td>
                    <td><?php echo $_POST['kosuu1'];?></td>
                    <td><?php echo $_POST['zeiritu1'];?></td>
                    <td><?php
                        $zeiritu1= $_POST['zeiritu1'] ;
                        $kakaku1= $_POST['kakaku1'] ;
                        $kosuu1= $_POST['kosuu1'] ;
                        $syoukei1 = $kakaku1 * $kosuu1*(($zeiritu1*0.01)+1);
                        echo number_format($syoukei1). '円(税込み)';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo $_POST['syouhinmei2'];?></td>
                    <td><?php echo $_POST['kakaku2'];?></td>
                    <td><?php echo $_POST['kosuu2'];?></td>
                    <td><?php echo $_POST['zeiritu2'];?></td>
                    <td>
                        <?php
                        $zeiritu2= $_POST['zeiritu2'] ;
                        $kakaku2= $_POST['kakaku2'] ;
                        $kosuu2= $_POST['kosuu2'] ;
                        $syoukei2 = $kakaku2 * $kosuu2*(($zeiritu2*0.01)+1);
                        echo number_format($syoukei2). '円(税込み)';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo $_POST['syouhinmei3'];?></td>
                    <td><?php echo $_POST['kakaku3'];?></td>
                    <td><?php echo $_POST['kosuu3'];?></td>
                    <td><?php echo $_POST['zeiritu3'];?></td>
                    <td>
                        <?php
                        $zeiritu3= $_POST['zeiritu3'] ;
                        $kakaku3= $_POST['kakaku3'] ;
                        $kosuu3= $_POST['kosuu3'] ;
                        $syoukei3 = $kakaku3 * $kosuu3*(($zeiritu3*0.01)+1);
                        echo number_format($syoukei3). '円(税込み)';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo $_POST['syouhinmei4'];?></td>
                    <td><?php echo $_POST['kakaku4'];?></td>
                    <td><?php echo $_POST['kosuu4'];?>
                    </td>
                    <td><?php echo $_POST['zeiritu4'];?></td>
                    <td>
                        <?php
                        $zeiritu4= $_POST['zeiritu4'] ;
                        $kakaku4= $_POST['kakaku4'] ;
                        $kosuu4= $_POST['kosuu4'] ;
                        $syoukei4 = $kakaku4 * $kosuu4*(($zeiritu4*0.01)+1);
                        echo number_format($syoukei4). '円(税込み)';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo $_POST['syouhinmei5'];?></td>
                    <td><?php echo $_POST['kakaku5'];?></td>
                    <td><?php echo $_POST['kosuu5'];?></td>
                    <td><?php echo $_POST['zeiritu5'];?></td>
                    <td>
                        <?php
                        $zeiritu5= $_POST['zeiritu5'] ;
                        $kakaku5= $_POST['kakaku5'] ;
                        $kosuu5= $_POST['kosuu5'] ;
                        $syoukei5 = $kakaku5 * $kosuu5*(($zeiritu5*0.01)+1);
                        echo number_format($syoukei5). '円(税込み)';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">合計</td>
                    <td>
                     <?php
                     $goukei=$syoukei1 + $syoukei2 + $syoukei3 + $syoukei4 + $syoukei5;
                     echo number_format($goukei). '円(税込み)';
                     ?>
                </tr>
            </table>
        </form>
    </body>
</html>
